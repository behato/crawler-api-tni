<?php

namespace App\Http\Controllers;

class CrawlerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function home(){
        return view("index");
    }
    public function getFungsiLogistik(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/fungsilogistik');
    }
    
    public function getJenisLogistik(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/jenislogistik');
    }
    
    public function getLogistik(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/logistik');
    }
    
    public function getKotama(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/kotama');
    }
    
    public function getOperasi(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/fungsilogistik');
    }
    
    public function getPerbatasan(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/perbatasan');
    }
    
    public function getPulauTerluar(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/pulauterluar');
    }
    
    public function getSatminkal(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/satminkal');
    }
    
    public function getCorpsPersonel(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/corpspersonel');
    }
    
    public function getPangkatPersonel(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/pangkatpersonel');
    }
    
    public function getPersonel(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/personel');
    }

    public function getDesa(){
        return exec('curl -X "GET" -H "Authorization:716ebf596d2e1d23fc90f7e86afce6c5e902a874" http://ekapaksi.hacks.id/api/v1/desa');
    }

    //
}
